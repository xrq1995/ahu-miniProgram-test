module.exports = [
  { name: '待处理', percent: 0.4, a: '1' },
  { name: '已分发', percent: 0.2, a: '1' },
  { name: '已取消', percent: 0.18, a: '1' },
  { name: '已派发', percent: 0.15, a: '1' },
  { name: '已完成', percent: 0.05, a: '1' },
  { name: '已评价', percent: 0.12, a: '1' }
];