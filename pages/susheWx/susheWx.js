// pages/fangwuWx/fangwuWx.js
var app = getApp();
var util = require("../../utils/util.js");
var api = require("../../utils/api.js");
Page({
  //页面的初始数据
  data: {

    repairDate: "",
    submitTime: "",
    tel: "",
    repairContext: "",
    repairTime: "",
    repairAddress: "array1+dormNumber",
    dormNumber: "",
    array1: ['请选择', '李园', '桃园', '桔园', '枫园', '槐园', '桂园', "竹园", "松园",
      "梅园", "杏园", "枣园", '榴园', '蕙园8栋', '蕙园9栋', '蕙园10栋'
    ],
    index1: 0,
    array2: [],
    Array: [],
    array3: [],
    index2: 0,
    index3: 0,
    array4: [],
    index4: 0,
    date: '',
    disabled: '',
    times: []
  },
  //页面初始化加载
  onLoad: function(options) {
    var that = this;
    var time = util.formatTime(new Date());
    var time1 = util.formatDate(new Date());
    that.setData({
      time: time,
      date: time1
    });
    var username = app.globalData.username;
    that.setData({
      number: username
    });
    that.setData({
      tel: '',
      dormNumber: '',
      repairContext: ''
    })
    //获取预约时间
    wx.request({
      url: api.repairTimeUrl,
      success(res) {
        var times = res.data.data;
        var str = [];
        var i = 0;
        var hour = util.formatHour(new Date());
        console.log(hour);
        switch (hour - 10) {
          case 0:
            i = 1;
            break;
          case 1:
            i = 1;
            break;
          case 2:
            i = 3;
            break;
          case 3:
            i = 3;
            break;
          case 4:
            i = 4;
            break;
          case 5:
            i = 4;
            break;
          case 6:
            i = 4;
            break;
          default:
            ((hour - 10) < 0 ? i = 0 : i = 6);
        };
        for (; i < times.length; i++) {
          str.push(times[i].startTime + '-' + times[i].endTime);
        }
        that.setData({
          array4: str
        })
        that.setData({
          times: times
        })
      }

    })
    //获取报修分类
    wx.request({
      url: api.repairCategoryUrl,
      success(res) {
        that.setData({
          repairCategory: res.data.data
        })
        var repairFenlei = that.data.repairCategory;
        var seriesData = [];
        var seriesData1 = [];
        for (var i = 0; i < repairFenlei.length; i++) {
          seriesData.push({
            id: i,
            array0: repairFenlei[i].parent,
            array: [repairFenlei[i].firstChild, repairFenlei[i].secondChild, repairFenlei[i].thirdChild, repairFenlei[i].fourthChild, repairFenlei[i].fifthChild]
          })
        }
        that.setData({
          Array1: seriesData
        })

        // 设置故障设备及故障分类
        var Array1 = that.data.Array1;
        var array2 = []
        for (var i = 0; i < Array1.length; i++) {
          array2.push(Array1[i].array0)
        }
        that.setData({
          array2: array2,
          array3: Array1[that.data.index2].array
        })
      }
    })
  },
  onReady: function() {

  },
  //清空表单
  onShow: function() {
    this.setData({
      tel: '',
      dormNumber: '',
      repairContext: ''
    })
  },
  //picker选择器
  listenerPickerSelected1: function(e) {
    //改变index值，通过setData()方法重绘界面
    this.setData({
      index1: e.detail.value
    });
  },

  // 故障分类 故障设备 二级联动
  listenerPickerSelected2: function(e) {
    this.setData({
      index2: e.detail.value,
      index3: 0
    })
    var Array1 = this.data.Array1
    this.setData({
      array3: Array1[this.data.index2].array
    })
  },

  listenerPickerSelected3: function(e) {
    this.setData({
      index3: e.detail.value
    });
  },
  listenerPickerSelected4: function(e) {
    this.setData({
      index4: e.detail.value
    });
  },
  bindDateChange: function(e) {
    var pickerDay = e.detail.value;
    pickerDay = pickerDay.substring(8, 10);
    var currentDay = util.formatDay(new Date());
    if (pickerDay > currentDay) {
      this.setData({
        date: e.detail.value
      })
      var str = [];
      var times = this.data.times;
      for (let i = 0; i < times.length - 1; i++) {
        str.push(times[i].startTime + '-' + times[i].endTime);
      }
      this.setData({
        array4: str,
      })
    } else {
      wx.showToast({
        title: '日期选择错误',
        icon: 'loading',
        duration: 1500
      })
      //判断是否有打开过页面
      if (getCurrentPages().length != 0) {
        //刷新当前页面的数据
        getCurrentPages()[getCurrentPages().length - 1].onLoad()
      }
    }
  },
  // 检查字符串是否为合法手机号码
  isPhone: function(str) {
    var reg = /^1(3|4|5|7|8|9)\d{9}$/;
    if (reg.test(str)) {
      return 1;
    } else {
      return -1;
    }
  },
  //提交表单数据操作函数  
  formSubmit: function(e) {
    this.setData({
      disabled: true
    })
    var that = this;
    //获取表单所有input的值
    var result = e.detail.value;
    var yuanqu = that.data.array1[that.data.index1];
    var repairAddress = yuanqu + result.dormNumber;
    var gzfl = that.data.array2[that.data.index2];
    var gzsb = that.data.array3[that.data.index3];
    var rqxz = that.data.date;
    var yysj = that.data.array4[that.data.index4];
    var repairType = gzfl + '-' + gzsb;

    if (this.isPhone(result.tel) < 0 || yuanqu.indexOf("请选择") > -1 || !result.dormNumber || !result.repairContext) {
      wx.showToast({
        title: '信息不完整',
        icon: "loading",
        duration: 1500
      })
      this.setData({
        disabled: false
      })
    } else if (yysj == '-') {
      wx.showToast({
        title: '请预约第二天',
        icon: "loading",
        duration: 1500
      })
      this.setData({
        disabled: false
      })
    } else {
      //将表单数据上传
      wx.request({
        url: api.uploadUrl,
        data: {
          repairDate: rqxz,
          submitTime: that.data.time,
          number: result.number,
          tel: result.tel,
          repairContext: result.repairContext,
          repairTime: yysj,
          repairAddress: repairAddress,
          repairType: repairType
        }, //表单数据
        method: 'POST',
        header: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        success: function(res) {
          console.log(res);
          if (res.data == 1) {
            wx.showToast({
              title: '今日预约已满',
              icon: "success",
              duration: 1500,
            })
          } else if (res.statusCode == 200) {
            wx.showToast({
              title: '提交成功',
              icon: "success",
              duration: 1500,
            });
            that.setData({
              tel: "",
              repairContext: '',
              dormNumber: '',
            })
            setTimeout(function() {
              wx.switchTab({
                url: '../index/index',
              })
            }, 2000)
          }
        },
        complete: function(res) {
          that.setData({
            disabled: false
          })
        },
      })
    }
  }
})