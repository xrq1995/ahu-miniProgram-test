import F2 from '../../../f2-canvas/lib/f2';

let chart = null;

function initChart(canvas, width, height) {
  const data = [{
    "year": 1997,
    "type": "新校区",
    "value": 4.9
  }, {
    "year": 1997,
    "type": "老校区",
    "value": 4.8
  }, {
    "year": 1998,
      "type": "新校区",
    "value": 4.5
  }, {
    "year": 1998,
      "type": "老校区",
    "value": 4.3
  }, {
    "year": 1999,
      "type": "新校区",
    "value": 4.2
  }, {
    "year": 1999,
      "type": "老校区",
    "value": 3.9
  }, {
    "year": 2000,
      "type": "新校区",
    "value": 4
  }, {
    "year": 2000,
      "type": "老校区",
    "value": 3.7
  }, {
    "year": 2001,
      "type": "新校区",
    "value": 4.7
  }, {
    "year": 2001,
      "type": "老校区",
    "value": 4.7
  }, {
    "year": 2002,
      "type": "新校区",
    "value": 5.8
  }, {
    "year": 2002,
      "type": "老校区",
    "value": 5.6
  }, {
    "year": 2003,
      "type": "新校区",
    "value": 6
  }, {
    "year": 2003,
      "type": "老校区",
    "value": 5.2
  }, {
    "year": 2004,
      "type": "新校区",
    "value": 5.5
  }, {
    "year": 2004,
      "type": "老校区",
    "value": 4.6
  }, {
    "year": 2005,
      "type": "新校区",
    "value": 5.1
  }, {
    "year": 2005,
      "type": "老校区",
    "value": 3.7
  }, {
    "year": 2006,
      "type": "新校区",
    "value": 4.6
  }, {
    "year": 2006,
      "type": "老校区",
    "value": 3.2
  }, {
    "year": 2007,
      "type": "新校区",
    "value": 4.6
  }, {
    "year": 2007,
      "type": "老校区",
    "value": 4
  }, {
    "year": 2008,
      "type": "新校区",
    "value": 5.8
  }, {
    "year": 2008,
      "type": "老校区",
    "value": 6.3
  }, {
    "year": 2009,
      "type": "新校区",
    "value": 7.9
  }, {
    "year": 2009,
      "type": "老校区",
    "value": 6.8
  }, {
    "year": 2010,
      "type": "新校区",
    "value": 8.6
  }, {
    "year": 2010,
      "type": "老校区",
    "value": 7.9
  }, {
    "year": 2011,
      "type": "新校区",
    "value": 8.9
  }, {
    "year": 2011,
      "type": "老校区",
    "value": 6.6
  }, {
    "year": 2012,
      "type": "新校区",
    "value": 8.1
  }, {
    "year": 2012,
      "type": "老校区",
    "value": 8.5
  }, {
    "year": 2013,
      "type": "新校区",
    "value": 7.4
  }, {
    "year": 2013,
      "type": "老校区",
    "value": 7.2
  }, {
    "year": 2014,
      "type": "新校区",
    "value": 6.2
  }, {
    "year": 2014,
      "type": "老校区",
    "value": 6.3
  }, {
    "year": 2015,
      "type": "新校区",
    "value": 8.3
  }, {
    "year": 2015,
      "type": "老校区",
    "value": 8.6
  }, {
    "year": 2016,
      "type": "新校区",
    "value": 8.8
  }, {
    "year": 2016,
      "type": "老校区",
    "value": 8.3
  }, {
    "year": 2017,
      "type": "新校区",
    "value": 9.1
  }, {
    "year": 2017,
      "type": "老校区",
    "value": 8.7
    },{
      "year": 2018,
      "type": "新校区",
      "value": 9.8
    }, {
      "year": 2018,
      "type": "老校区",
      "value": 9.3
    }];

  chart = new F2.Chart({
    el: canvas,
    width,
    height
  });

  chart.source(data, {
    year: {
      range: [0, 1],
      ticks: [1997, 1999, 2001, 2003, 2005, 2007, 2009, 2011, 2013, 2015, 2017]
    },
    value: {
      tickCount: 10,
      formatter(val) {
        return val.toFixed(1) + '%';
      }
    }
  });

  chart.tooltip({
    custom: true, // 自定义 tooltip 内容框
    onChange(obj) {
      const legend = chart.get('legendController').legends.top[0];
      const tooltipItems = obj.items;
      const legendItems = legend.items;
      const map = {};
      legendItems.map(item => {
        map[item.name] = Object.assign({}, item);
      });
      tooltipItems.map(item => {
        const {
          name,
          value
        } = item;
        if (map[name]) {
          map[name].value = value;
        }
      });
      legend.setItems(Object.values(map));
    },
    onHide() {
      const legend = chart.get('legendController').legends.top[0];
      legend.setItems(chart.getLegendItems().country);
    }
  });

  chart.guide().rect({
    start: [2011, 'max'],
    end: ['max', 'min'],
    style: {
      fill: '#CCD6EC',
      opacity: 0.3
    }
  });
  chart.guide().text({
    position: [2014, 'max'],
    content: 'Scott administratio\n(2011 to present)',
    style: {
      fontSize: 10,
      textBaseline: 'top'
    }
  });

  chart.line().position('year*value').color('type', val => {
    if (val === 'United States') {
      return '#ccc';
    }
  });
  chart.render();
  return chart;
}

Page({
  onShareAppMessage: function(res) {
    return {
      title: 'F2 微信小程序图表组件，你值得拥有~',
      path: '/pages/index/index',
      success: function() {},
      fail: function() {}
    }
  },
  data: {
    opts: {
      onInit: initChart
    }
  },

  onReady() {}
});