import F2 from '../../../f2-canvas/lib/f2';
import data from '../../../data/ring.js'
let chart = null;

function initChart(canvas, width, height) {
  const {
    Util,
    G
  } = F2;
  const {
    Group
  } = G;

  function drawLabel(shape, coord, canvas) {
    const {
      center
    } = coord;
    const origin = shape.get('origin');
    const points = origin.points;
    const x1 = (points[2].x - points[1].x) * 0.75 + points[1].x;
    const x2 = (points[2].x - points[1].x) * 1.8 + points[1].x;
    const y = (points[0].y + points[1].y) / 2;
    const point1 = coord.convertPoint({
      x: x1,
      y
    });
    const point2 = coord.convertPoint({
      x: x2,
      y
    });

    // const group = new Group();
    const group = canvas.addGroup();
    group.addShape('Line', {
      attrs: {
        x1: point1.x,
        y1: point1.y,
        x2: point2.x,
        y2: point2.y,
        lineDash: [0, 2, 2],
        stroke: '#808080'
      }
    });
    const text = group.addShape('Text', {
      attrs: {
        x: point2.x,
        y: point2.y,
        text: origin._origin.type + '  ' + origin._origin.cost + ' 件',
        fill: '#808080',
        textAlign: 'left',
        textBaseline: 'bottom'
      }
    });
    const textWidth = text.getBBox().width;
    const baseLine = group.addShape('Line', {
      attrs: {
        x1: point2.x,
        y1: point2.y,
        x2: point2.x,
        y2: point2.y,
        stroke: '#808080'
      }
    });
    if (point2.x > center.x) {
      baseLine.attr('x2', point2.x + textWidth);
    } else if (point2.x < center.x) {
      text.attr('textAlign', 'right');
      baseLine.attr('x2', point2.x - textWidth);
    } else {
      text.attr('textAlign', 'center');
      text.attr('textBaseline', 'top');
    }
    //canvas.add(group);
    shape.label = group;
  }

  let sum = 0;
  data.map(obj => {
    sum += obj.cost;
  });
  chart = new F2.Chart({
    el: canvas,
    width,
    height
  });
  chart.source(data);
  let lastClickedShape;
  chart.legend({
    position: 'bottom',
    offsetY: -5,
    marker: 'square',
    align: 'center',
    itemMarginBottom: 20,
    onClick(ev) {
      const {
        clickedItem
      } = ev;
      const dataValue = clickedItem.get('dataValue');
      const canvas = chart.get('canvas');
      const coord = chart.get('coord');
      const geom = chart.get('geoms')[0];
      const container = geom.get('container');
      const shapes = geom.get('shapes'); // 只有带精细动画的 geom 才有 shapes 这个属性

      let clickedShape;
      // 找到被点击的 shape
      Util.each(shapes, shape => {
        const origin = shape.get('origin');
        if (origin && origin._origin.type === dataValue) {
          clickedShape = shape;
          return false;
        }
      });

      if (lastClickedShape) {
        lastClickedShape.animate().to({
          attrs: {
            lineWidth: 0
          },
          duration: 200
        }).onStart(() => {
          if (lastClickedShape.label) {
            lastClickedShape.label.hide();
          }
        }).onEnd(() => {
          lastClickedShape.set('selected', false);
        });
      }

      if (clickedShape.get('selected')) {
        clickedShape.animate().to({
          attrs: {
            lineWidth: 0
          },
          duration: 200
        }).onStart(() => {
          if (clickedShape.label) {
            clickedShape.label.hide();
          }
        }).onEnd(() => {
          clickedShape.set('selected', false);
        });
      } else {
        const color = clickedShape.attr('fill');
        clickedShape.animate().to({
          attrs: {
            lineWidth: 5
          },
          duration: 350,
          easing: 'bounceOut'
        }).onStart(() => {
          clickedShape.attr('stroke', color);
          clickedShape.set('zIndex', 1);
          container.sort();
        }).onEnd(() => {
          clickedShape.set('selected', true);
          clickedShape.set('zIndex', 0);
          container.sort();
          lastClickedShape = clickedShape;
          if (clickedShape.label) {
            clickedShape.label.show();
          } else {
            drawLabel(clickedShape, coord, canvas);
          }
          canvas.draw();
        });
      }
    }
  });
  chart.coord('polar', {
    transposed: true,
    innerRadius: 0.7,
    radius: 0.5
  });
  chart.axis(false);
  chart.tooltip(false);
  chart.interval()
    .position('a*cost')
    .color('type', ['#1890FF', '#13C2C2', '#2FC25B', '#FACC14', '#F04864', '#8543E0'])
    .adjust('stack');

  chart.guide().text({
    position: ['50%', '50%'],
    content: sum.toFixed(2),
    style: {
      fontSize: 24
    }
  });
  chart.render();
  return chart;
}

Page({
  onShareAppMessage: function(res) {
    return {
      title: 'F2 微信小程序图表组件，你值得拥有~',
      path: '/pages/index/index',
      success: function() {},
      fail: function() {}
    }
  },
  data: {
    opts: {
      onInit: initChart
    }
  },

  onLoad() {

  },
  onReady() {},
});


// import F2 from '../../../f2-canvas/lib/f2';

// Page({
//   onShareAppMessage: function(res) {
//     return {
//       title: 'F2 微信小程序图表组件，你值得拥有~',
//       path: '/pages/index/index',
//       success: function() {},
//       fail: function() {}
//     }
//   },
//   data: {
//     optspie: {}, // 资金占比饼状图
//   },

//   // 小程序页面加载之前
//   onLoad() {
//     const that = this // 绑定好this指向

//     // 资金占比模块-资金占比饼状图
//     let pie = null // 先声明一个变量用以后面做F2的new
//     function pieChart(canvas, width, height) { // F2实现回调的方法，方法名用来最后赋值绑定
//       //这里是为让请求接口返回数据能直接赋值给到图表数据渲染，所以用的ES6写法
//       new Promise(function(resolve, reject) {
//         wx.request({
//           url: 'https://antv.alipay.com/assets/data/candle-sticks.json',
//           // 请求成功后执行
//           success: function(res) {
//             // const dataNum = res.data.response.day_fund_info;

//               // 这里是将返回的字符数据转换为数字以方便图表
//               const large_deal_amount = 10
//               const mid_deal_amount = 20
//               const small_deal_amount = 30
//               // 赋值对应值给到饼图（也可以把percent给改成纯数字这样你就能在自己的项目上看到效果了）
//               let pieData = [{
//                   name: '主力',
//                   percent: 200,
//                 },
//                 {
//                   name: '跟风',
//                   percent: 200,
//                 },
//                 {
//                   name: '散户',
//                   percent: 300,
//                 }
//               ]
//               resolve(pieData) // 将数据返回给到new上进行then索取
            
//           }
//         })
//       }).then((data) => {
//         const total = 100 // 计算总数
//         let brunt = 20// 计算主力占比
//         let Retail = 50 // 计算跟风占比
//         let follow = 30 // 计算散户占比
//         // 饼图的图例配置
//         const map = {
//           '主力': '20%',
//           '跟风': '20%',
//           '散户': '20%'
//         };
//         // 刚刚声明的变量就是用在这里，new到F2的指定
//         pie = new F2.Chart({
//           el: canvas,
//           width,
//           height
//         });
//         pie.source(data); // data就是传入的数据。给到F2
//         // 图例位置
//         pie.legend({
//           position: 'right', // 放在右边展示
//           itemFormatter(val) { //配置图例自定义展示内容
//             return val + '  ' + map[val];
//           },
//           nameStyle: {
//             fontSize: '13', // 图例文本大小
//             fill: '#ffffff' // 图例文本颜色
//           },
//           marker: {
//             symbol: 'circle', // marker 的形状（圆点）
//             radius: 4 // 半径大小
//           }
//         });
//         pie.tooltip(false); // 是否显示工具箱
//         pie.coord('polar', { // 匹配饼状图的元素圆度等
//           transposed: true,
//           radius: 0.85,
//           innerRadius: 0.618
//         });
//         pie.axis(false); // 关闭XY轴等线条
//         pie.interval()
//           .position('a*percent') // a为默认，percent即数据中的percent数值
//           .color('name', ['#FF4381', '#F2B356', '#F25E54']) // 饼图对应颜色
//           .adjust('stack')
//           .style({
//             lineWidth: 1,
//             stroke: '#fff',
//             lineJoin: 'round',
//             lineCap: 'round'
//           });
//         pie.interaction('pie-select', {
//           cancelable: true, // 允许取消选中
//           animate: { // 选中动画效果
//             duration: 300,
//             easing: 'backOut'
//           }
//         });
//         pie.render(); // 执行
//       })
//       return pie; //最后返回给到pie
//     }
//     this.setData({
//       optspie: {
//         onInit: pieChart //这里就是在js中用到的方法名
//       }
//     })
//   }
// });