import F2 from '../../../f2-canvas/lib/f2';

let chart = null;

function initChart(canvas, width, height) {
  // 饼图 map
  const map = {
    '蕙园': '40%',
    '桔园': '20%',
    '桂园': '18%',
    '枫园': '15%',
    '槐园': '5%',
    '其他': '2%',
  };
  // 饼图 数据 选中交互
  const data = [
    { name: '蕙园', percent: 0.4, a: '1' },
    { name: '桔园', percent: 0.2, a: '1' },
    { name: '桂园', percent: 0.18, a: '1' },
    { name: '枫园', percent: 0.15, a: '1' },
    { name: '槐园', percent: 0.05, a: '1' },
    { name: '其他', percent: 0.02, a: '1' }
  ];
  chart = new F2.Chart({
    el: canvas,
    width,
    height
  });
  chart.source(data, {
    percent: {
      formatter(val) {
        return val * 100 + '%';
      }
    }
  });
  chart.legend({
    position: 'right',
    itemFormatter(val) {
      return val + '  ' + map[val];
    }
  });
  chart.tooltip(false);
  chart.coord('polar', {
    transposed: true,
    radius: 0.85
  });
  chart.axis(false);
  chart.interval()
    .position('a*percent')
    .color('name', ['#1890FF', '#13C2C2', '#2FC25B', '#FACC14', '#F04864', '#8543E0'])
    .adjust('stack')
    .style({
      lineWidth: 1,
      stroke: '#fff',
      lineJoin: 'round',
      lineCap: 'round'
    })
    .animate({
      appear: {
        duration: 1200,
        easing: 'bounceOut'
      }
    });

  chart.render();
  return chart;
}

Page({
  onShareAppMessage: function (res) {
    return {
      title: 'F2 微信小程序图表组件，你值得拥有~',
      path: '/pages/index/index',
      success: function () { },
      fail: function () { }
    }
  },
  data: {
    opts: {
      onInit: initChart
    }
  },

  onReady() {
  }
});
