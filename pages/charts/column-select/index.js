import F2 from '../../../f2-canvas/lib/f2';

let chart = null;
// 柱状图
function initChart(canvas, width, height) {
  var data = [
    { year: '李 园', sales: 1038 },
    { year: '桃 园', sales: 952 },
    { year: '桔 园', sales: 861 },
    { year: '枫 园', sales: 1145 },
    { year: '槐 园', sales: 948 },
    { year: '桂 园', sales: 838 },
    { year: '竹 园', sales: 912 },
    { year: '松 园', sales: 741 },
    { year: '杏 园', sales: 669 },
    { year: '枣 园', sales: 369 },
    { year: '惠 8', sales: 569 }, 
    { year: '惠 9', sales: 459 },
    { year: '惠 10', sales: 349 },

  ];
  chart = new F2.Chart({
    el: canvas,
    width,
    height
  });

  chart.source(data, {
    sales: {
      tickCount: 5
    }
  });
  chart.tooltip(false);
  chart.interval().position('year*sales');
  chart.render();

  // 绘制柱状图文本
  const offset = -5;
  const chartCanvas = chart.get('canvas');
  const group = chartCanvas.addGroup();
  const shapes = {};
  data.map(obj => {
    const point = chart.getPosition(obj);
    const text = group.addShape('text', {
      attrs: {
        x: point.x,
        y: point.y + offset,
        text: obj.sales,
        textAlign: 'center',
        textBaseline: 'bottom',
        fill: '#808080'
      }
    });

    shapes[obj.year] = text; // 缓存该 shape, 便于后续查找
  });

  let lastTextShape; // 上一个被选中的 text
  // 配置柱状图点击交互
  chart.interaction('interval-select', {
    selectAxisStyle: {
      fill: '#000',
      fontWeight: 'bold'
    },
    mode: 'range',
    onEnd(ev) {
      const { data, selected } = ev;
      lastTextShape && lastTextShape.attr({
        fill: '#808080',
        fontWeight: 'normal'
      });
      if (selected) {
        const textShape = shapes[data.year];
        textShape.attr({
          fill: '#000',
          fontWeight: 'bold'
        });
        lastTextShape = textShape;
      }
      chartCanvas.draw();
    }
  });
  return chart;
}

Page({
  onShareAppMessage: function (res) {
    return {
      title: 'F2 微信小程序图表组件，你值得拥有~',
      path: '/pages/index/index',
      success: function () { },
      fail: function () { }
    }
  },
  data: {
    opts: {
      onInit: initChart
    }
  },

  onReady() {
  }
});
