// pages/query/query.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    cateItems: [
      {
        cate_id: 1,
        cate_name: "常用",
        ishaveChild: true,
        children:
          [
            {
              child_id: 1,
              name: '网络服务中心',
              tel: '0551-63861118'
            },
            {
              child_id: 2,
              name: '一卡通办公室',
              tel: '0551-63861077'
            },
            {
              child_id: 3,
              name: '报警电话',
              tel: '0551-63861110'
            },
            {
              child_id: 4,
              name: '校医院值班电话',
              tel: '0551-63861120'
            },
            {
              child_id: 5,
              name: '物业办公室',
              tel: '0551-63861044'
            },
            {
              child_id: 6,
              name: '物业修缮服务室',
              tel: '0551-63861114'
            },
            {
              child_id: 7,
              name: '考试考务中心',
              tel: '0551-63861053'
            },
            {
              child_id: 8,
              name: '学生资助管理中心',
              tel: '0551-63861008'
            },
            {
              child_id: 9,
              name: '大学生勤俭中心',
              tel: '0551-63861181'
            },
            {
              child_id: 10,
              name: '图书馆',
              tel: '0551-63861109'
            }
          ]
      },
      {
        cate_id: 2,
        cate_name: "教务处",
        ishaveChild: true,
        children:
          [
            {
              child_id: 1,
              name: '综合办公室',
              tel: '0551-63861005'
            },
            {
              child_id: 2,
              name: '考试考务中心',
              tel: '0551-63861053'
            },
            {
              child_id: 3,
              name: '教学质量科',
              tel: '0551-63861235'
            },
            {
              child_id: 4,
              name: '考试考务中心',
              tel: '0551-63861053'
            },
            {
              child_id: 5,
              name: '教学运行中心',
              tel: '0551-63861203'
            },
            {
              child_id: 6,
              name: '学籍管理科',
              tel: '0551-63861202'
            },
          ]
      },
      {
        cate_id: 3,
        cate_name: "团委",
        ishaveChild: true,
        children:
          [
            {
              child_id: 1,
              name: '团委办公室',
              tel: '0551-63861121'
            },
            {
              child_id: 2,
              name: '创服中心',
              tel: '0551-63861550'
            },
            {
              child_id: 3,
              name: '社团联合员会',
              tel: '0551-63861182'
            },
            {
              child_id: 4,
              name: '大学生勤俭中心',
              tel: '0551-63861181'
            },
            {
              child_id: 5,
              name: '团学宣传中心',
              tel: '0551-63861662'
            },
          ]
      },
      {
        cate_id: 4,
        cate_name: "学生处",
        ishaveChild: true,
        children: [
          {
            child_id: 1,
            name: '学生思想教育科',
            tel: '0551-63861054'
          },
          {
            child_id: 2,
            name: '学生管理科',
            tel: '0551-63861900'
          },
          {
            child_id: 3,
            name: '学生资助管理中心',
            tel: '0551-63861008'
          },
          {
            child_id: 4,
            name: '就业指导中心',
            tel: '0551-63861355'
          },
        ]
      },
      {
        cate_id: 5,
        cate_name: "财务处",
        ishaveChild: true,
        children:
          [
            {
              child_id: 1,
              name: '办公室',
              tel: '0551-63861569'
            },
            {
              child_id: 2,
              name: '收费管理处',
              tel: '0551-63861561'
            },
          ]
      },
      {
        cate_id: 6,
        cate_name: "保卫处",
        ishaveChild: true,
        children:
          [
            {
              child_id: 1,
              name: '办公室',
              tel: '0551-63861224'
            },
            {
              child_id: 2,
              name: '户籍室',
              tel: '0551-63861184'
            },
            {
              child_id: 3,
              name: '报警电话',
              tel: '0551-63861110'
            },
          ]
      },
      {
        cate_id: 7,
        cate_name: "宿舍",
        ishaveChild: true,
        children:
          [
            {
              child_id: 1,
              name: '桃园',
              tel: '0551-63861034'
            },
            {
              child_id: 2,
              name: '李园',
              tel: '0551-63861037'
            },
            {
              child_id: 3,
              name: '桔园',
              tel: '0551-63861036'
            },
            {
              child_id: 4,
              name: '枣园',
              tel: '0551-63861218'
            },
            {
              child_id: 5,
              name: '榴园',
              tel: '0551-63861217'
            },
            {
              child_id: 6,
              name: '杏园',
              tel: '0551-63861219'
            },
            {
              child_id: 7,
              name: '松园',
              tel: '0551-63861160'
            },
            {
              child_id: 8,
              name: '竹园',
              tel: '0551-63861115'
            },
            {
              child_id: 9,
              name: '梅园',
              tel: '0551-63861113'
            },
            {
              child_id: 10,
              name: '桂园',
              tel: '0551-63861097'
            },
            {
              child_id: 11,
              name: '枫园',
              tel: '0551-63861096'
            },
            {
              child_id: 12,
              name: '槐园',
              tel: '0551-63861081'
            }
          ]
      },
      {
        cate_id: 8,
        cate_name: "物业处",
        ishaveChild: true,
        children:
          [
            {
              child_id: 1,
              name: '办公室',
              tel: '0551-63861004'
            },
            {
              child_id: 2,
              name: '修缮服务室',
              tel: '0551-63861114'
            },
          ]
      },
      {
        cate_id: 9,
        cate_name: "校医院",
        ishaveChild: true,
        children:
          [
            {
              child_id: 1,
              name: '24小时值班电话',
              tel: '0551-63861120'
            },
            {
              child_id: 2,
              name: '校医疗保障办公室',
              tel: '0551-65108781'
            },
          ]
      },
    ],
    curNav: 1,
    curIndex: 0
  },
  //事件处理函数  
  switchRightTab: function (e) {
    // 获取item项的id，和数组的下标值  
    let id = e.target.dataset.id,
      index = parseInt(e.target.dataset.index);
    // 把点击到的某一项，设为当前index  
    this.setData({
      curNav: id,
      curIndex: index
    })
  },
  //拨打电话
  callContact: function (e) {
    wx.makePhoneCall({
      phoneNumber: e.currentTarget.dataset.phonenumber
    })
  }
})
