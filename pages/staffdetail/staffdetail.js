// pages/staffdetail/staffdetail.js
// var API_URL = 'http://localhost:8080/user/query/';

Page({

  /**
   * 页面的初始数据
   */
  data: {
    staff: [
      {
        id: 'E012001', photo: 'http://img.027cgb.com/596971/repair.jpg', name: "陆建国", sex: '男', work: '部主任', rating: 9.9, number: '13905513282', place: '后勤总部'
      },
      { id: 'E012002', photo: 'http://img.027cgb.com/596971/repair.jpg', name: "蒋顺奎", sex: '男', work: '主管', rating: 9.8, number: '13956019527', place: '后勤总部' },
      { id: 'E012003', photo: 'http://img.027cgb.com/596971/repair.jpg', name: "张林", sex: '男', work: '主管', rating: 9.8, number: '13965092398', place: '后勤总部' },
      { id: 'E012004', photo: 'http://img.027cgb.com/596971/repair.jpg', name: "林永安", sex: '男', work: '木工', rating: 9.8, number: '13505695397', place: '桃 李 桔' },
      { id: 'E012005', photo: 'http://img.027cgb.com/596971/repair.jpg', name: "姚敬东", sex: '男', work: '空调', rating: 9.8, number: '13505695397', place: '新、老校区公寓' },
      { id: 'E012006', photo: 'http://img.027cgb.com/596971/repair.jpg', name: "鞠文平", sex: '男', work: '水电', rating: 9.8, number: '13856062341', place: '磬苑校区' },
      { id: 'E012007', photo: 'http://img.027cgb.com/596971/repair.jpg', name: "吴修宏", sex: '男', work: '水电', rating: 9.8, number: '13966791557', place: '蕙园10 周转房 北门' },
      { id: 'E012008', photo: 'http://img.027cgb.com/596971/repair.jpg', name: "彭发根", sex: '男', work: '水电', rating: 9.8, number: '13696522748', place: '行政楼 艺术楼 南体育场 东西南门' },
      { id: 'E012009', photo: 'http://img.027cgb.com/596971/repair.jpg', name: "袁天胜", sex: '男', work: '水电', rating: 9.8, number: '13866114117', place: '仓库管理' },
      { id: 'E012010', photo: 'http://img.027cgb.com/596971/repair.jpg', name: "黄强", sex: '男', work: '水电', rating: 9.8, number: '13095511079', place: '松、竹、梅园' },
      { id: 'E012011', photo: 'http://img.027cgb.com/596971/repair.jpg', name: "王解兵", sex: '男', work: '瓦工', rating: 9.8, number: '13855144764', place: '协调机动' },
      { id: 'E012012', photo: 'http://img.027cgb.com/596971/repair.jpg', name: "王秀强", sex: '男', work: '水电', rating: 9.8, number: '13866143741', place: '枫、桂、槐园' },
      { id: 'E012013', photo: 'http://img.027cgb.com/596971/repair.jpg', name: "吴伟毅", sex: '男', work: '电焊', rating: 9.8, number: '13855136430', place: '磬苑校区' },
      { id: 'E012014', photo: 'http://img.027cgb.com/596971/repair.jpg', name: "金石", sex: '男', work: '木工', rating: 9.8, number: '13505697033', place: '枫 槐 桂 行政楼 艺术楼' },
      { id: 'E012015', photo: 'http://img.027cgb.com/596971/repair.jpg', name: "赵国勇", sex: '男', work: '电工', rating: 9.8, number: '13866134977', place: '协调机动' },
      { id: 'E012016', photo: 'http://img.027cgb.com/596971/repair.jpg', name: "周明华", sex: '男', work: '瓦工', rating: 9.8, number: '13966708566', place: '磬苑校区' },
      { id: 'E012017', photo: 'http://img.027cgb.com/596971/repair.jpg', name: "安明瑞", sex: '男', work: '瓦工', rating: 9.8, number: '13866712478', place: '磬苑校区' },
      { id: 'E012018', photo: 'http://img.027cgb.com/596971/repair.jpg', name: "刘保成", sex: '男', work: '水电', rating: 9.8, number: '15255153729', place: '路灯 食堂' },
      { id: 'E012019', photo: 'http://img.027cgb.com/596971/repair.jpg', name: "李琦", sex: '男', work: '水电', rating: 9.8, number: '15256915119', place: '磬苑校区' },
      { id: 'E012020', photo: 'http://img.027cgb.com/596971/repair.jpg', name: "潘祖定", sex: '男', work: '木工', rating: 9.8, number: '13856937069', place: '松 竹 梅 适之楼 校医院' },
      { id: 'E012021', photo: 'http://img.027cgb.com/596971/repair.jpg', name: "严纪文", sex: '男', work: '瓦工', rating: 9.8, number: '15240012495', place: '磬苑校区' },
      { id: 'E012022', photo: 'http://img.027cgb.com/596971/repair.jpg', name: "张志强", sex: '男', work: '木工', rating: 9.8, number: '18005603500', place: '榴 枣 杏园' },
      { id: 'E012023', photo: 'http://img.027cgb.com/596971/repair.jpg', name: "屠德毅", sex: '男', work: '水电', rating: 9.8, number: '15056040533', place: '蕙园9 北体育场' },
      { id: 'E012024', photo: 'http://img.027cgb.com/596971/repair.jpg', name: "张祖明", sex: '男', work: '瓦工', rating: 9.8, number: '15856381955', place: '磬苑校区' },
      { id: 'E012025', photo: 'http://img.027cgb.com/596971/repair.jpg', name: "曹安春", sex: '男', work: '电焊', rating: 9.8, number: '13355601097', place: '磬苑校区' },
      { id: 'E012026', photo: 'http://img.027cgb.com/596971/repair.jpg', name: "金家保", sex: '男', work: '水电', rating: 9.8, number: '13865934867', place: '榴 枣园' },
      { id: 'E012027', photo: 'http://img.027cgb.com/596971/repair.jpg', name: "陈长茂", sex: '男', work: '水电', rating: 9.8, number: '13956955561', place: '蕙园8 体育馆' },
      { id: 'E012028', photo: 'http://img.027cgb.com/596971/repair.jpg', name: "徐明圣", sex: '男', work: '水电', rating: 9.8, number: '18255121153', place: '桔 适之楼 校医院 中心办公室' },
      { id: 'E012029', photo: 'http://img.027cgb.com/596971/repair.jpg', name: "吴俊海", sex: '男', work: '水电', rating: 9.8, number: '13155114612', place: '桃 李 行知楼' },
      { id: 'E012030', photo: 'http://img.027cgb.com/596971/repair.jpg', name: "王海东", sex: '男', work: '水电', rating: 9.8, number: '17333220121', place: '杏 园 校医院' },
      { id: 'E012031', photo: 'http://img.027cgb.com/596971/repair.jpg', name: "吴建军", sex: '男', work: '水电', rating: 9.8, number: '15395171497', place: '路灯 食堂' },
    ],
    index:null,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

      this.setData({
        index:options.index
      });
    // var that = this;
    // wx.request({
    //   url: '../staffinfo/staffinfo.js',
    //   data: {},
    //   header: {
    //     'Content-Type': 'json'
    //   },
    //   success: function (res) {
    //     console.log(res.data)
    //     that.setData({
    //       staff: res.data
    //     });
    //   }
    // })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})