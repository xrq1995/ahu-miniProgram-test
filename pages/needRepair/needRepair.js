// pages/needRepair/needRepair.js
var api = require("../../utils/api.js");
var app = getApp();
var util = require('../../utils/util.js');
Page({
  data: {
    navbar: ['待处理维修信息'],
    currentTab: 0
  },
  /**
   * 生命周期函数--监听页面加载 一个页面只会调用一次，可以在 onLoad 中获取打开当前页面所调用的 query 参数。
   */
  onLoad: function(options) {
    var that = this;
    var repairName = options.name;
    // 获取全部报修记录
    wx.request({
      url: api.repairworkerUrl,
      method: 'GET',
      data: {
        repairName: repairName
      },
      success: (res) => {
        // var repairInfo = res.data.data;
        var repairInfo = res.data.data.reverse();
        // var repairMsg = repairInfo;
        var repairMsg = repairInfo.slice(0, 100);
        // console.log(res.data);
        // console.log(repairMsg)
        var repairedMsg = [];
        for (var key in repairMsg) {
          repairMsg[key].submitTime = repairMsg[key].submitTime;
          // repairMsg[key].submitTime = util.formatTime(new Date(repairMsg[key].submitTime));
          repairMsg[key].repairTime = repairMsg[key].repairDate + '\t' + repairMsg[key].repairTime;
          if (repairMsg[key].repairStatus == '待处理' || repairMsg[key].repairStatus == '已派发') {
            repairedMsg.push(repairMsg[key]);
          }

        }
        that.setData({
          repeiredMsgData: repairedMsg
        })
        // console.log(this.data.repeirMsgData);
      },
      fail: (res) => {

      }
    })
  },
  /**
   * 生命周期函数--监听页面显示。每次打开页面都会调用一次
   */
  onShow: function () {
    // this.onLoad();
  },
  /**
   * 将时间戳转换成日期 方式1
   */
  getLocalTime: function (nS) {
    return new Date(parseInt(nS)).toLocaleString().replace(/:\d{1,2}$/, ' ');
  },
  /**
    * 将时间戳转换成日期 方式2
    */
  timestampToTime: function (timestamp) {
    var date = new Date(timestamp);
    var Y = date.getFullYear() + '-';
    var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth + 1) + '-';
    var D = date.getDate() + ' ';
    var h = date.getHours() + ':';
    var m = date.getMinutes() + ':';
    var s = date.getSeconds();
    if (s > 0 & s < 10) {
      s = '0' + s;
    }
    return Y + M + D + h + m + s;
  },

  /**
   * 顶部导航栏点击
   */
  navbarTap: function (e) {

  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  /**
   * 点击完成订单
   */
  repairDone: function (e) {
    var that = this;
    var cancelData = this.data.repeiredMsgData;
    console.log(cancelData)
    wx.showModal({
      title: '提示',
      content: '确定完成订单吗？',
      success: function (res) {
        if (res.confirm) {
          console.log('用户点击确定')
          var index = e.currentTarget.dataset.index;
          console.log(index)
          console.log(cancelData[index].repairOrderNumber)
          cancelData[index].repairStatus = '已完成';
          // 发起请求 取消订单
          wx.request({
            url: api.repairDoneUrl,
            method: 'GET',
            data: {
              id: cancelData[index].id,
              repairOrderNumber: cancelData[index].repairOrderNumber
            },
            success: (res) => {
              wx.showToast({
                title: '报修完成',
                icon: 'success',
                duration:2000,
                success:function(){
                  wx.navigateBack({
                    delta:1
                  })
                }
              })
            },
            error: (res) => {
              console.log(res)
            }
          })

        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },

})