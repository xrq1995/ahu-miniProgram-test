// pages/creator/creator.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    cateItems: [
      {
        cate_id: 1,
        cate_name: "产品",
        ishaveChild: true,
        children:
          [
            {
              child_id: 1,
              name: '许瑞卿',
              tel: 861995928
            },
            {
              child_id: 2,
              name: '徐伟伟',
              tel: 123456789
            },
            {
              child_id: 3,
              name: '徐殷',
              tel: 123456789
            }
          ]
      },
      {
        cate_id: 2,
        cate_name: "前端",
        ishaveChild: true,
        children:
          [
            {
              child_id: 1,
              name: '许瑞卿',
              tel: 861995928
            },
            {
              child_id: 2,
              name: '徐殷',
              tel: 123456789
            },
          ]
      },
      {
        cate_id: 3,
        cate_name: "后端",
        ishaveChild: true,
        children:
          [
            {
              child_id: 1,
              name: '许瑞卿',
              tel: 861995928
            },
            {
              child_id: 2,
              name: '钱鑫',
              tel: 123456789
            },
            {
              child_id: 3,
              name: '徐伟伟',
              tel: 123456789
            },
            {
              child_id: 4,
              name: '徐殷',
              tel: 123456789
            },
          ]
      },
      {
        cate_id: 4,
        cate_name: "设计",
        ishaveChild: true,
        children: [
          {
            child_id: 1,
            name: '刘卫雅',
            tel: 123456789
          },
          {
            child_id: 2,
            name: '宋荣洁',
            tel: 123456789
          },
          {
            child_id: 3,
            name: '冯佳欣',
            tel: 123456789
          },
        ]
      },
      {
        cate_id: 5,
        cate_name: "鸣谢",
        ishaveChild: true,
        children: 
        [
            {
              child_id: 1,
              name: '安徽大学后服中心',
              tel: 253878334
            },
        ]
      }
    ],
    curNav: 1,
    curIndex: 0
  },
  //事件处理函数  
  switchRightTab: function (e) {
    // 获取item项的id，和数组的下标值  
    let id = e.target.dataset.id,
      index = parseInt(e.target.dataset.index);
    // 把点击到的某一项，设为当前index  
    this.setData({
      curNav: id,
      curIndex: index
    })
  }
})