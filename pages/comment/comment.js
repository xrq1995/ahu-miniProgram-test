// pages/comment/comment.js
var app = getApp();
var util = require("../../utils/util.js");
var api = require("../../utils/api.js");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderNumber: '',
    array1: ['好评', '中评', '差评'
    ],
    index1: 0,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this
    this.setData({
      orderNumber: options.orderNumber
    })
    console.log(this.data.orderNumber);

    var user = app.globalData.username;
    that.setData({
      number: user
    })
  },

  /**
   * 满意度选择器
   */
  listenerPickerSelected1: function(e) {
    //改变index值，通过setData()方法重绘界面
    this.setData({
      index1: e.detail.value
    });
  },
  /**
   * 提交数据
   */
  formSubmit: function(e) {
    var that = this;
    var result = e.detail.value;
    var orderNumber = this.data.orderNumber;
    var satisfaction = this.data.array1[this.data.index1];
    console.log(result);
    console.log(satisfaction);
    console.log(orderNumber);

    var time = new Date();
    that.setData({
      time: time
    });
    if (result.number.length != 9) {
      wx.showToast({
        title: '学工号有误！',
        duration: 1500
      })
    } else {
      // 请求评论接口
      wx.request({
        url: api.repairCommentUrl,
        data: {
          // number: result.number,
          repairOrderNumber: orderNumber,
          // username: result.username,
          satisfaction: satisfaction,
          // tel: result.telnumber,
          comment: result.comment,
          // submittime: that.data.time
        },
        method: 'POST',
        header: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        success: function(res) {
          console.log(res)
          if (res.statusCode == 200) {
            wx.showToast({
              title: '反馈成功',
              duration: 2000
            })
          } else {
            wx.showToast({
              title: '提交失败',
              duration: 2000
            })
          }
          setTimeout(function() {
            wx.switchTab({
              url: '../bxrecord/bxrecord',
            })
          }, 1500)
        },
        fail: function(res) {
          wx.showToast({
            title: '网络异常',
            duration: 2000
          })
        }
      })
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },

})