// pages/susherecord/susherecord.js
var app = getApp();
var util = require("../../utils/util.js");
var api = require("../../utils/api.js");
Page({
  /**
   * 页面的初始数据
   */
  data: {
    number: "",
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this
    var user = app.globalData.username;
    that.setData({
      number: user
    })
    /**   
     * 加载申请数据
     */
    wx.request({
      url: api.dormRecordUrl,
      data: {
        number: app.globalData.username,
      },
      method: 'GET',
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function(res) {
        console.log(res);
        if (res.data == 1 || res.data == 0) {
              wx.showToast({
                title: '没有申请权限',
                duration: 500
              })
              setTimeout(function () {
                wx.switchTab({
                  url: '../index/index',
                })
              }, 1000)
          // wx.showModal({
          //   title: '没有申请权限',
          //   showCancel: false,
          //   success: function(res) {
          //     if (res.confirm) {
          //       wx.switchTab({
          //         url: '../index/index',
          //       })
          //     }
          //   }
          // })
        } else
        if (res.data.applytime == null) {  //按照申请日期是否为空作为判断 是否合理
          wx.showToast({
            title: '无申请记录',
            duration: 1500
          })
        } else {
          that.setData({
            number: res.data.number,
            name: res.data.name,
            sex: res.data.sex,
            tel: res.data.tel,
            zoom: res.data.zoom,
            doornumber: res.data.doornumber,
            applystatus: res.data.applystatus,
            applycontext: res.data.applycontext,
            // applytime: util.formatTime(new Date(res.data.applytime)),
            applytime: res.data.applytime
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },

  //宿舍申请
  submit1: function() {
    var that = this;
    var month = util.formatMonth(new Date());
    var day = util.formatDay(new Date());
    if (month == '08' && '00'< day && day < '11') {   //指定申请日期 8月1日到8月10日
        wx.navigateTo({
          url: '../susheSq/susheSq',
        })
    } else {
      wx.showModal({
        title: '指定日期开放',
        showCancel: false,
        success: function(res) {
          if (res.confirm) {
            wx.switchTab({
              url: '../index/index',
            })
          }
        }
      })
    }
  }
})