// pages/susheSq/susheSq.js
var app = getApp();
var util = require("../../utils/util.js");
var api = require("../../utils/api.js");
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    var user = app.globalData.username;
    that.setData({
      number: user
    })
  },

  /**
   * 提交按钮提交数据
   */
  formSubmit: function(e) {
    var that = this;
    var result = e.detail.value;

    // if (result.number.length != 9) {
    //   wx.showToast({
    //     title: '学工号有误！',
    //     duration: 1500
    //   })
    // } 
    // else
    if (result.tel.length != 11) {
      wx.showToast({
        title: '手机号码有误！',
        duration: 1500
      })
    } 
    else {
      wx.request({
        url: api.dormApplyUrl,
        data: {
          number: result.number,
          name: result.name,
          tel: result.tel,
          applycontext: result.applycontext,
        },
        method: 'POST',
        header: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        success: function(res) {
          if(res.data == 0){
            wx.showToast({
            title: '学号姓名不匹配',
              duration: 1500
            })
          }else{
          wx.showToast({
            title: '操作成功',
            duration: 1500
          })
          setTimeout(function() {
            wx.switchTab({
              url: '../index/index',
            })
          }, 1000)
          }
        },
        fail: function(res) {
          console.log('操作失败')
         }
       })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
