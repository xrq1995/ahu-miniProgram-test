//index.js
//获取应用实例
var app = getApp()
var md = require('../../utils/md5.js')

var utils = require('../../utils/util.js')
Page({
  data: {
    savedFilePath: "",
    username: getApp().globalData.username,
    password: "",
    code: "",
    // 滚动图
    imgUrls: [
      'http://img.027cgb.com/596971/ahu.jpg',
      'http://img.027cgb.com/596971/ahu1.jpg',
    ],
    // 工具第一行
    arr1: [{
        imgurl: '../../images/icon/staffinfo.png',
        txt: '维修工'
      },
      {
        imgurl: '../../images/icon/weather.png',
        txt: '天气'
      },
      {
        imgurl: '../../images/icon/query.png',
        txt: '校内电话'
      },
      {
        imgurl: '../../images/icon/data.png',
        txt: '数据'
      }
    ],
    // 工具第二行
    arr2: [{
        imgurl: '../../images/icon/susheSq.png',
        txt: '宿舍申请'
      },
      {
        imgurl: '../../images/icon/kongtiao.png',
        txt: '空调故障'
      },
      {
        imgurl: '../../images/icon/rl.png',
        txt: '浴室'
      },
      {
        imgurl: '../../images/icon/lost-found.png',
        txt: '失物招领'
      }
    ],
    // 工具第三行
    arr3: [{
        imgurl: '../../images/icon/pay.png',
        txt: '校园缴费'
      },
      {
        imgurl: '../../images/icon/kd.png',
        txt: '快递'
      },
      {
        imgurl: '../../images/icon/jw.png',
        txt: '教务处'
      },
      {
        imgurl: '../../images/icon/library.png',
        txt: '图书馆'
      },
    ]
  },
  onLoad: function(options) {
    var self = this;
    var systemInfo = wx.getSystemInfoSync();
    self.setData({
      username: getApp().globalData.username,
    })
  },

  //点击跳转相应的页面  跳转到非tabBar页面用navigateTo  tarBar用switchTab
  kindToggle: function(e) {
    var txt = e.currentTarget.id;
    switch (txt) {
      case '公告':
        wx.navigateTo({
          url: '../notice/notice'
        });
        break;
      case '数据':
        wx.navigateTo({
          url: '../index-echarts/index'
        })
        break;
      case '天气':
        wx.navigateTo({
          url: '../kefu/kefu'
        });
        break;
      case '校内电话':
        wx.navigateTo({
          url: '../query/query'
        });
        break;
      case '宿舍申请':
        wx.navigateTo({
          url: '../susherecord/susherecord'
        });
        break;
      case '空调故障':
        wx.navigateTo({
          url: '../airCondition/airCondition'
        });
        break;
      case '浴室':
        wx.navigateTo({
          url: '../calendar/calendar'
        });
        break;
      case '失物招领':
        wx.navigateTo({
          url: '../lost-found/lost-found'
        });
        break;
      case '维修工':
        wx.navigateTo({
          url: '../staffinfo/staffinfo'
        });
        break;
      case '校园缴费':
        wx.navigateTo({
          url: '../pay/pay'
        });
        break;
      case '快递':
        wx.navigateToMiniProgram({
          appId: 'wxe1e1654acfeebb3e',
          success(res) {
            console.log(res);
            // 打开成功
          }
        })
        break;
      case '教务处':
        wx.navigateTo({
          url: '../jwc/jwc'
        });
        break;
      case '图书馆':
        wx.navigateTo({
          url: '../library/library'
        });
        break;
    }
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})