var api = require("../../utils/api.js");
var app = getApp();
var util = require('../../utils/util.js');
Page({
  data: {
    navbar: ['全部', '进行中', '已完成'],
    currentTab: 0
  },
  /**
   * 生命周期函数--监听页面加载 一个页面只会调用一次，可以在 onLoad 中获取打开当前页面所调用的 query 参数。
   */
  onLoad() {
    this.setData({
      number: getApp().globalData.username
    })

    var number = this.data.number;
    var that = this;
    // 获取全部报修记录
    wx.request({
      url: api.repairRecordUrl,
      method: 'GET',
      data: {
        number: app.globalData.username
      },
      success: (res) => {
        var repairMsg = res.data.data;
        repairMsg = repairMsg.reverse();
        console.log(res.data);
        for (var key in repairMsg) {
          repairMsg[key].submitTime = repairMsg[key].submitTime;
          // repairMsg[key].submitTime = util.formatTime(new Date(repairMsg[key].submitTime));
          repairMsg[key].repairTime = repairMsg[key].repairDate + '\t' + repairMsg[key].repairTime;
        }
        that.setData({
          repeirMsgData: repairMsg
        })
        // console.log(this.data.repeirMsgData);
      },
      fail: (res) => {

      }
    })
  },
  /**
   * 生命周期函数--监听页面显示。每次打开页面都会调用一次
   */
  onShow: function () {
    this.onLoad();
  },
  /**
   * 将时间戳转换成日期 方式1
   */
  getLocalTime: function (nS) {
    return new Date(parseInt(nS)).toLocaleString().replace(/:\d{1,2}$/, ' ');
  },
  /**
    * 将时间戳转换成日期 方式2
    */
  timestampToTime: function (timestamp) {
    var date = new Date(timestamp);
    var Y = date.getFullYear() + '-';
    var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth + 1) + '-';
    var D = date.getDate() + ' ';
    var h = date.getHours() + ':';
    var m = date.getMinutes() + ':';
    var s = date.getSeconds();
    if (s > 0 & s < 10) {
      s = '0' + s;
    }
    return Y + M + D + h + m + s;
  },
  /**
   * 暂不支持取消保修
   */
  // cancel: function (e) {
  //   wx.showModal({
  //     title: '提示',
  //     content: '暂不开通取消操作',
  //     success: function (res) {
  //       if (res.confirm) {
  //         console.log('用户点击确定')
  //       } else if (res.cancel) {
  //         console.log('用户点击取消')
  //       }
  //     }
  //   })
  // },

  /**
   * 评价功能
   */
  comment: function (e) {
    var that = this;
    var orderData = this.data.repairMsg2;
    wx.showModal({
      title: '提示',
      content: '确定评价订单吗？',
      success: function (res) {
        if (res.confirm) {
          console.log('用户点击确定')
          var index = e.currentTarget.dataset.index;
          // 跳转到 评论页面
          var orderNumber = orderData[index].repairOrderNumber;
          console.log(orderNumber);
          wx.navigateTo({
            url: '../comment/comment?orderNumber=' + orderNumber,
          })
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },
  /**
   * 顶部导航栏点击
   */
  navbarTap: function (e) {
    // var that = this;
    this.setData({
      currentTab: e.currentTarget.dataset.idx
    })
    switch (this.data.currentTab) {
      case 1:

        var repairMsg1 = [];
        for (var key in this.data.repeirMsgData) {
          if (this.data.repeirMsgData[key].repairStatus == '待处理' || this.data.repeirMsgData[key].repairStatus == '已派发') {
            repairMsg1.push(this.data.repeirMsgData[key])
          }
        }
        this.setData({
          repairMsg1: repairMsg1
        })
        console.log(this.data.repairMsg1)

        break;
      case 2:
        var repairMsg2 = [];
        for (var key in this.data.repeirMsgData) {
          if (this.data.repeirMsgData[key].repairStatus == '已完成') {
            repairMsg2.push(this.data.repeirMsgData[key])
          }
        }
        this.setData({
          repairMsg2: repairMsg2
        })
        console.log(this.data.repairMsg2)
        break;
      case 3:
        var repairMsg3 = [];
        for (var key in this.data.repeirMsgData) {
          if (this.data.repeirMsgData[key].repairStatus == '已取消') {
            repairMsg3.push(this.data.repeirMsgData[key])
          }
        }
        this.setData({
          repairMsg3: repairMsg3
        })
        console.log(this.data.repairMsg3)
        break;
    }
    console.log(this.data.currentTab)
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  /**
   * 取消报修
   */
  // cancel: function (e) {
  //   var that = this;
  //   var cancelData = this.data.repairMsg1;
  //   wx.showModal({
  //     title: '提示',
  //     content: '确定取消订单吗？',
  //     success: function (res) {
  //       if (res.confirm) {
  //         console.log('用户点击确定')
  //         var index = e.currentTarget.dataset.index;
  //         console.log(cancelData[index].repairOrderNumber)
  //         // cancelData[index].repairStatus = '已取消';
  //         // 发起请求 取消订单
  //         // wx.request({
  //         //   url: api.repairCancelUrl,
  //         //   method: 'GET',
  //         //   data: {
  //         //     id: cancelData[index].id,
  //         //     repairOrderNumber: cancelData[index].repairOrderNumber
  //         //   },
  //         //   success: (res) => {
  //         //     wx.showToast({
  //         //       title: '取消成功',
  //         //       icon: 'success',
  //         //     })
  //         //     console.log(res);
  //         //     wx.clearStorage(that.data.repairMsg)
  //         //     that.setData({
  //         //       repeirMsgData: res.data
  //         //     });
  //         //     console.log(index);
  //         //   },
  //         //   error: (res) => {
  //         //     console.log(res)
  //         //   }
  //         // })

  //       } else if (res.cancel) {
  //         console.log('用户点击取消')
  //       }
  //     }
  //   })
  // },

})