// pages/lost-found/lost-found.js
var api = require("../../utils/api.js");
var that;
var app = getApp();
var adds = {};
Page({
  data: {
    image_width: 0,
    nickName: "",
    avatarUrl: "",
    loading: false,
    images: [],
    urlArr: [],
    publishList: ["拾到物品", "丢失物品"],
    reasonList: ["卡类", "钥匙", "数码产品", "U盘", "其他物品"],
    idP: null,
    idR: null,
    choseReason: '',
    formdata: '',
    content: '',
    tel: "",
    extracontent: '',
  },

  onLoad: function () {
    that = this;
    wx.getSystemInfo({
      success: function (res) {
        // console.log(res);
        that.setData({
          image_width: (res.windowWidth / 4) - 10
        });
      }
    });
  },

  bindSubmit: function (e) {
    // 判断是否正在上传图片
    // if (that.data.loading) {
    // 	return;
    // }
    var reason = that.data.choseReason;
    var content = e.detail.value.content;
    var nickName = app.globalData.wxuserInfo.nickName;
    var publish = that.data.chosePublish;
    var tel = e.detail.value.tel;
    if (!reason) {
      wx.showToast({
        title: '请选择物品类型',
        image: '../../images/more/about.png',
        duration: 2000
      })
    }
    else if (!content) {
      wx.showToast({
        title: '请填写拾到/丢失地点',
        image: '../../images/more/about.png',
        duration: 2000
      })
    } else if (!tel) {
      wx.showToast({
        title: '请填写正确的联系方式',
        image: '../../images/more/about.png',
        duration: 2000
      })
    }
    else {
      nickName = app.globalData.wxuserInfo.nickName;
      publish = that.data.chosePublish;
      reason = that.data.choseReason;
      content = e.detail.value.content;//拾到或者丢失地点
      that.setData({
        content: e.detail.value.content,
        extracontent: e.detail.value.extracontent,
        tel: e.detail.value.tel
      })
      this.uploadViews()
    }
  },
  chosePublish: function (e) {
    var index1 = e.currentTarget.dataset.index;  //获取自定义的ID值  
    this.setData({
      idP: index1,
      chosePublish: that.data.publishList[index1]
    })
    console.log(that.data.idP);
    // console.log(that.data.chosePublish);
  },

  choseReason: function (e) {
    var index2 = e.currentTarget.dataset.index;  //获取自定义的ID值  
    this.setData({
      idR: index2,
      choseReason: that.data.reasonList[index2]
    })
  },

  upImg: function () {
    var that = this;
    wx.chooseImage({
      count: 9, // 默认9
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        wx.showNavigationBarLoading()
        that.setData({
          loading: false
        })
        var urlArr = that.data.urlArr;
        // var urlArr={};
        var tempFilePaths = res.tempFilePaths;
        var images = that.data.images;
        // console.log(tempFilePaths);
        that.setData({
          images: images.concat(tempFilePaths),
        });
      }
    })
    console.log(that.data.urlArr)
  },
  uploadViews: function () {
    var that = this
    if (this.data.images.length == 0) {
      wx.request({
        url: api.lostfoundNoImgUrl,
        data: {
          nickName: app.globalData.wxuserInfo.nickName,
          publish: that.data.chosePublish,
          reason: that.data.choseReason,
          content: that.data.content,
          extracontent: that.data.extracontent,
          tel: that.data.tel,
        }, //表单数据
        method: 'POST',
        header: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        name: 'views',
        success: function (res) {
          console.log(res);
          if (res.statusCode == 200) {
            wx.showToast({
              title: '反馈成功，感谢！',
              icon: "success",
              duration: 1500,
            })
          } else {
            wx.showToast({
              title: '提交失败',
              icon: "fail",
              duration: 1500,
            });
            that.setData({
              reason: '',
              content: '',
            })

          }
          setTimeout(function () {
            wx.switchTab({
              url: '../index/index',
            })
          }, 2000)
        },
      })
    } else {
      for (var i = 0; i < this.data.images.length; i++) {
        wx.uploadFile({
          url: api.lostfoundUrl,
          filePath: that.data.images[i],
          name: 'lostfound',
          formData: {
            'nickName': app.globalData.wxuserInfo.nickName,
            'publish': that.data.chosePublish,
            'reason': that.data.choseReason,
            'content': that.data.content,
            'extracontent': that.data.extracontent,
            'tel': that.data.tel,
          },
          header: {
            'content-type': 'multipart/form-data'
          },
          success: function (res) {
            console.log(res)
            if (res.statusCode == 200) {
              wx.showToast({
                title: '反馈成功，感谢！',
                duration: 1500
              });
            } else {
              wx.showToast({
                title: '提交失败',
                icon: "fail",
                duration: 1500,
              });
            }
            setTimeout(function () {
              wx.switchTab({
                url: '../index/index',
              })
            }, 2000)
          }

        })
        this.setData({
          adds: {},
        })
      }
    }
    this.setData({
      adds: {},
      images: ''
    })
  },

 // 获取本地显示的图片数组
  delete: function (e) {
    var index = e.currentTarget.dataset.index;
    var images = that.data.images;
    var urlArr = that.data.urlArr;
    urlArr.splice(index, 1);
    images.splice(index, 1);
    that.setData({
      images: images,
      urlArr: urlArr
    });
    console.log(that.data.urlArr)
  },

  // 检查字符串是否为合法手机号码
  isPhone: function (str) {
    var reg = /^1(3|4|5|7|8|9)\d{9}$/;
    if (reg.test(str)) {
      return 1;
    } else {
      return -1;
    }
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})