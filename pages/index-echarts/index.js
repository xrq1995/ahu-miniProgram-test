Page({
  data: {
    charts: [    
      { name: 'line', value: '折线图' },
      { name: 'column', value: '柱状图' },
      { name: 'bar', value: '条形图' },
      { name: 'dodge', value: '分组柱状图' },
      { name: 'stackBar', value: '层叠条形图' },
      { name: 'ring', value: '环图' },
      { name: 'pie', value: '饼图' },
     
    ],
    others: [
      { name: 'scroll-line', value: '线图平移交互(长按展示 tooltip)' },
      { name: 'steps-pan', value: '每日步数（柱状图平移）' },
      { name: 'pie-select', value: '饼图选中交互' },
      { name: 'column-select', value: '各园区报修量-柱状图选中交互(可取消选中)' },
      { name: 'gradient-column', value: '渐变色柱状图' }
    ]
  },
  gotoPage: function (e) {
    var page = e.currentTarget.dataset.page;
    console.log(e.currentTarget)
    console.log(e.currentTarget.dataset)
    console.log(page);
    wx.navigateTo({
      url: '../charts/' + page + '/index'
    });
  },
  onLoad: function () {

  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})