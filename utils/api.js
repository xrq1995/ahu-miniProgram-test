const baseurl = "https://ahurepair.cn/repair/v1"

const api = {
  loginUrl: baseurl + '/userinfo',
  uploadUrl: baseurl + '/upload',
  repairCategoryUrl: baseurl + '/repaircategories',
  repairRecordUrl: baseurl + '/inquireinfo',
  repairTimeUrl: baseurl + '/repairtime',
  repairCancelUrl: baseurl + '/cancel',
  repairCommentUrl: baseurl + '/comment',
  dormRecordUrl: baseurl + '/dormrecord',
  dormApplyUrl: baseurl + '/dormapply',
  viewsUrl: baseurl + '/dormapply',
  lostfoundUrl: baseurl + '/lostfound',
  lostfoundNoImgUrl: baseurl + '/lostfoundNoImg',
  repairworkerUrl: baseurl + '/repairworker',       //维修工端 报修记录查询接口
  repairDoneUrl: baseurl + '/repairdone'            //点击完成维修单
}
module.exports = api;